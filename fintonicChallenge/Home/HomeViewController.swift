//
//  HomeViewController.swift
//  fintonicChallenge
//
//  Created by Jose Eduardo Gutierrez Pastrana on 12/1/19.
//  Copyright © 2019 Jose Eduardo Gutierrez Pastrana. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UITableViewDataSource, UITableViewDelegate, WebClientDelegate {
    
    @IBOutlet weak var homeCollectionView: UICollectionView!
    @IBOutlet weak var myFavoritesView: UIView!
    @IBOutlet weak var myFavoritesListView: UIView!
    @IBOutlet weak var favoritesTableView: UITableView!
    
    let overlay = OverlayViewController(nibName: "OverlayViewController", bundle: nil)
    
    var superHerosArray:[Superhero] = []
    var superHerosFavorites:[Superhero]=[]
    var superHerosImages:[String:UIImage] = [:]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        initData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        loadFavorites()
    }
    
    func setupUI(){
        self.myFavoritesView.layer.cornerRadius = 12
        self.myFavoritesListView.layer.cornerRadius = 12
               
        self.homeCollectionView.register(UINib(nibName: "HomeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HomeCollectionCell")
        self.favoritesTableView.register(UINib(nibName: "FavoriteTableViewCell", bundle: nil), forCellReuseIdentifier: "FavoriteTableViewCell")
               
    }
    
    func initData(){
        WebClientManager.client.delegate = self
        WebClientManager.client.getSuperherosData()
        overlay.modalPresentationStyle = .fullScreen
        present(overlay, animated: true, completion: nil)
    }
    
    /// Load the stored images
    func loadPhotos(){
        for superhero in self.superHerosArray {
            if let image = DBManager.manager.getStoreImage(identifier: superhero.name!){
                self.superHerosImages.updateValue(image, forKey: superhero.name!)
            }
        }
    }
    
    /// Load the favorites superheros
    func loadFavorites(){
        self.superHerosFavorites.removeAll()
        for superhero in self.superHerosArray{
            if superhero.favorite{
                self.superHerosFavorites.append(superhero)
            }
        }
        
        self.favoritesTableView.reloadData()
    }
    

    ///
    /// CollectionView DataSource
    ///
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.superHerosArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let identifier = "HomeCollectionCell"
        let cell: HomeCollectionViewCell? = self.homeCollectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as? HomeCollectionViewCell
        
        let superhero: Superhero  = self.superHerosArray[indexPath.row]
        let superHeroName:String = superhero.name!
        cell?.nameLabel.text = superHeroName
        cell?.realNameLabel.text = superhero.realName
        
        
        if let image = self.superHerosImages[superHeroName]{
            cell?.superHeroImageView.image = image
            cell?.imageActivity.isHidden = true
        }else{
            if let imgURL = superhero.photo{
                WebClientManager.client.downloadImage(imageURL: imgURL, imageIdentifierName: superHeroName, imageViewUpdate: cell!.superHeroImageView, placeholderView: cell!.imageActivity)
            }
        }
        
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let superhero = self.superHerosArray[indexPath.row]
        let detailVC = SuperheroDetailViewController(nibName: "SuperheroDetailViewController", bundle: nil)
        detailVC.superhero = superhero
        detailVC.photo = self.superHerosImages[superhero.name!] ?? UIImage()
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
    
    ///
    /// TableView Datasource and Delegate
    ///
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.superHerosFavorites.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "FavoriteTableViewCell"
        var cell: FavoriteTableViewCell? = self.favoritesTableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? FavoriteTableViewCell
        if cell == nil {
            cell = self.favoritesTableView.dequeueReusableCell(withIdentifier: identifier) as? FavoriteTableViewCell
        }
        
        let superHero = self.superHerosFavorites[indexPath.row]
        cell?.nameLabel.text = superHero.name
        
        if let image = self.superHerosImages[superHero.name!]{
            cell?.superheroImageview.image = image
            cell?.imageActivity.isHidden = true
        }else{
            if let imgURL = superHero.photo{
                WebClientManager.client.downloadImage(imageURL: imgURL, imageIdentifierName: superHero.name!, imageViewUpdate: cell!.superheroImageview, placeholderView: cell!.imageActivity)
            }
        }
        
        
        return cell!
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let superhero = self.superHerosFavorites[indexPath.row]
        let detailVC = SuperheroDetailViewController(nibName: "SuperheroDetailViewController", bundle: nil)
        detailVC.superhero = superhero
        detailVC.photo = self.superHerosImages[superhero.name!] ?? UIImage()
        self.navigationController?.pushViewController(detailVC, animated: true)
    }

    ///
    /// WebClient Delegate
    ///
    
    func finishGetSuperheroData(isSuccess: Bool, superHerosArray: [[String : Any]]?) {
        if isSuccess{
            DBManager.manager.saveSuperheros(superherosData: superHerosArray!)
        }
        self.superHerosArray = DBManager.manager.getSuperheros()
        loadPhotos()
        self.homeCollectionView.reloadData()
        self.loadFavorites()
        overlay.finishOverlay()
    }
    
    func finishDownloadImage(image: UIImage?, imageIdentifierName: String?) {
        if image != nil {
            let name:String = imageIdentifierName ?? "unknow"
            self.superHerosImages.updateValue(image!, forKey: name)
            DBManager.manager.storeImage(image: image!, withIdentifier: name)
        }else{
            let name:String = imageIdentifierName ?? "unknow"
            self.superHerosImages.updateValue(UIImage(named: "not_image")!, forKey: name)
        }
        
    }
}
