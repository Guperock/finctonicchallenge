//
//  FavoriteTableViewCell.swift
//  fintonicChallenge
//
//  Created by Jose Eduardo Gutierrez Pastrana on 12/1/19.
//  Copyright © 2019 Jose Eduardo Gutierrez Pastrana. All rights reserved.
//

import UIKit

class FavoriteTableViewCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var superheroImageview: UIImageView!
    @IBOutlet weak var imageActivity: UIActivityIndicatorView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        superheroImageview.layer.cornerRadius = superheroImageview.frame.height/2
        superheroImageview.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
