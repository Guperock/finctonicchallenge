//
//  HomeCollectionViewCell.swift
//  fintonicChallenge
//
//  Created by Jose Eduardo Gutierrez Pastrana on 12/1/19.
//  Copyright © 2019 Jose Eduardo Gutierrez Pastrana. All rights reserved.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var superHeroImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var realNameLabel: UILabel!
    @IBOutlet weak var imageActivity: UIActivityIndicatorView!
    @IBOutlet weak var openButton: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.superHeroImageView.layer.cornerRadius = self.superHeroImageView.frame.height/2
        self.superHeroImageView.clipsToBounds = true
        self.cellView.layer.cornerRadius = 12
        self.openButton.layer.cornerRadius = self.openButton.frame.height/2
        self.openButton.clipsToBounds = true
        
    }
    
    
    

}
