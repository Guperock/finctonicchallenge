//
//  SuperheroDetailViewController.swift
//  fintonicChallenge
//
//  Created by Jose Eduardo Gutierrez Pastrana on 12/1/19.
//  Copyright © 2019 Jose Eduardo Gutierrez Pastrana. All rights reserved.
//

import UIKit

class SuperheroDetailViewController: UIViewController {

    @IBOutlet weak var superheroImageView: UIImageView!
    @IBOutlet weak var superheroNameLabel: UILabel!
    @IBOutlet weak var realNameLabel: UILabel!
    @IBOutlet weak var heigthLabel: UILabel!
    @IBOutlet weak var powerLabel: UILabel!
    @IBOutlet weak var abilitiesLabel: UILabel!
    @IBOutlet weak var groupsLabel: UILabel!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var bodyView: UIView!
    @IBOutlet weak var favoriteButton: UIButton!
    
    var superhero: Superhero!
    var photo:UIImage = UIImage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI(){
        self.headerView.layer.cornerRadius = 12
        
          self.bodyView.layer.cornerRadius = 12
          
          self.superheroImageView.image = photo
          self.superheroImageView.layer.cornerRadius = self.superheroImageView.frame.height/2
          
          self.superheroImageView.clipsToBounds = true
          self.superheroNameLabel.text = self.superhero.name
          self.realNameLabel.text = self.superhero.realName
          self.heigthLabel.text = self.superhero.height
          self.powerLabel.text = self.superhero.power
          self.abilitiesLabel.text = self.superhero.abilities
          self.groupsLabel.text = self.superhero.groups
          
          if superhero.favorite{
              self.favoriteButton.setBackgroundImage(UIImage(systemName: "heart.fill"), for: .normal)
              self.favoriteButton.tintColor = UIColor.red
          }
          
          self.navigationController?.navigationBar.tintColor = UIColor.black
    }


    @IBAction func favoriteClicked(_ sender: Any) {
        if superhero.favorite {
            self.favoriteButton.setBackgroundImage(UIImage(systemName: "heart"), for: .normal)
            self.favoriteButton.tintColor = UIColor.lightGray
            superhero.favorite = false
        }else{
            self.favoriteButton.setBackgroundImage(UIImage(systemName: "heart.fill"), for: .normal)
            self.favoriteButton.tintColor = UIColor.red
            superhero.favorite = true
        }
        
    }
    
}
