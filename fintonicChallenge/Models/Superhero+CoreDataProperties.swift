//
//  Superhero+CoreDataProperties.swift
//  fintonicChallenge
//
//  Created by Jose Eduardo Gutierrez Pastrana on 11/27/19.
//  Copyright © 2019 Jose Eduardo Gutierrez Pastrana. All rights reserved.
//
//

import Foundation
import CoreData


extension Superhero {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Superhero> {
        return NSFetchRequest<Superhero>(entityName: "Superhero")
    }

    @NSManaged public var name: String?
    @NSManaged public var photo: String?
    @NSManaged public var realName: String?
    @NSManaged public var height: String?
    @NSManaged public var power: String?
    @NSManaged public var abilities: String?
    @NSManaged public var groups: String?
    @NSManaged public var favorite: Bool

}
