//
//  Superhero+CoreDataClass.swift
//  fintonicChallenge
//
//  Created by Jose Eduardo Gutierrez Pastrana on 11/27/19.
//  Copyright © 2019 Jose Eduardo Gutierrez Pastrana. All rights reserved.
//
//

import Foundation
import CoreData

@objc(Superhero)
public class Superhero: NSManagedObject {
    
    static let entityName = "Superhero"
       
    func create(data: [String: Any], context:NSManagedObjectContext) -> Superhero {
        let newItem = NSEntityDescription.insertNewObject(forEntityName: Superhero.entityName, into: context) as! Superhero
        
        newItem.name = data["name"] as? String ?? "Unknow"
        newItem.photo = data["photo"] as? String ?? ""
        newItem.realName = data["realName"] as? String ?? "Unknow"
        newItem.height = data["height"] as? String ?? "Unknow"
        newItem.power = data["power"] as? String ?? "Unknow"
        newItem.abilities = data["abilities"] as? String ?? "Unknow"
        newItem.groups = data["groups"] as? String ?? "Unknow"
        newItem.favorite = false
           
        return newItem
           
    }
    
    func get(withPredicate queryPredicate: NSPredicate, context: NSManagedObjectContext) -> [Superhero]{
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: Superhero.entityName)
        
        fetchRequest.predicate = queryPredicate
        fetchRequest.returnsObjectsAsFaults = false
        
        do {
            let response = try context.fetch(fetchRequest)
            return response as! [Superhero]
            
        } catch let error as NSError {
            // failure
            print(error)
            return [Superhero]()
        }
    }
    
    func getById(id: NSManagedObjectID, context: NSManagedObjectContext) -> Superhero? {
           return context.object(with: id) as? Superhero
       }
    
    func save(data: [String: Any], context: NSManagedObjectContext) -> Superhero {
        var saved_item: Superhero?
        let the_name: String = data["name"] as! String
        let the_predicate: NSPredicate = NSPredicate(format: "name == %@", the_name)
        
        let results: [Superhero] = get(withPredicate: the_predicate, context: context)
        
        if (results.count > 0) { // is an update
            let the_item: Superhero = results[0]
            saved_item = update(toupdatedObject: the_item, data: data, context: context)
        } else {
            saved_item = create(data: data, context: context)
        }
        
        
        return saved_item!
    }
    
    func update(toupdatedObject: Superhero, data: [String: Any], context: NSManagedObjectContext) -> Superhero{
        let item: Superhero?
        item = Superhero(entity: NSEntityDescription.entity(forEntityName: Superhero.entityName, in: context)!, insertInto: nil)
        if let item = getById(id: toupdatedObject.objectID, context: context){
            item.name = data["name"] as? String ?? "Unknow"
            item.photo = data["photo"] as? String ?? ""
            item.realName = data["realName"] as? String ?? "Unknow"
            item.height = data["height"] as? String ?? "Unknow"
            item.power = data["power"] as? String ?? "Unknow"
            item.abilities = data["abilities"] as? String ?? "Unknow"
            item.groups = data["groups"] as? String ?? "Unknow"
        }
        
        return item!
    }
    
    // Deletes an item
    func delete(id: NSManagedObjectID, context: NSManagedObjectContext){
        if let objectToDelete = getById(id: id, context: context){
            context.delete(objectToDelete)
        }
    }
    
    func getAll(context: NSManagedObjectContext) -> [Superhero]{
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: Superhero.entityName)
           
           fetchRequest.predicate = NSPredicate(value:true)
           fetchRequest.returnsObjectsAsFaults = false
           
           do {
               let response = try context.fetch(fetchRequest)
               return response as! [Superhero]
               
           } catch let error as NSError {
               // failure
               print(error)
               return [Superhero]()
           }
       }

}
