//
//  DBManager.swift
//  fintonicChallenge
//
//  Created by Jose Eduardo Gutierrez Pastrana on 12/1/19.
//  Copyright © 2019 Jose Eduardo Gutierrez Pastrana. All rights reserved.
//

import UIKit
import CoreData

/// Class to manage all querys and writes to the DB
class DBManager: NSObject {
    
    /// Singleton of the class
    public static let manager = DBManager()
    
    let context: NSManagedObjectContext = {
        return (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    }()
    
    private override init() {
    }
    
    /// Save in the DB a superheros array
    func saveSuperheros(superherosData: [[String:Any]]){
        let db_superheros = Superhero(entity: NSEntityDescription.entity(forEntityName: Superhero.entityName, in: self.context)!, insertInto: nil)
        for superhero in superherosData{
            _ = db_superheros.save(data: superhero, context: self.context)
        }
        do{
            try self.context.save()
        } catch let error as NSError {
            // failure
            print(error)
        }
    }
    
    /// Get all superheros from the DB
    func getSuperheros()->[Superhero]{
        let db_superheros = Superhero(entity: NSEntityDescription.entity(forEntityName: Superhero.entityName, in: self.context)!, insertInto: nil)
        
        return db_superheros.getAll(context: self.context)
    }
    
    func storeImage(image: UIImage, withIdentifier identifier:String){
        if let imageData = image.jpegData(compressionQuality: 1){
            if let filePath = fileImagePath(forIdentifier: identifier) {
                do  {
                    try imageData.write(to: filePath,
                                                options: .atomic)
                } catch let err {
                    print("Saving file resulted in error: ", err)
                }
            }
        }
    }
    
    
    /// Create the path to store images jpg
    private func fileImagePath(forIdentifier key: String) -> URL? {
        let fileManager = FileManager.default
        guard let documentURL = fileManager.urls(for: .documentDirectory,
                                                in: FileManager.SearchPathDomainMask.userDomainMask).first else { return nil }
        
        return documentURL.appendingPathComponent(key + ".jpg")
    }
    
    /// Return UIImage from a image if is stored
    func getStoreImage(identifier:String)-> UIImage?{
        if let filePath = self.fileImagePath(forIdentifier: identifier),
            let fileData = FileManager.default.contents(atPath: filePath.path),
            let image = UIImage(data: fileData) {
            return image
        }
        
        return nil
    }

}
