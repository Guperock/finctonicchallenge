//
//  OverlayViewController.swift
//  fintonicChallenge
//
//  Created by Jose Eduardo Gutierrez Pastrana on 12/2/19.
//  Copyright © 2019 Jose Eduardo Gutierrez Pastrana. All rights reserved.
//

import UIKit

/// Overlay to show while waiting
class OverlayViewController: UIViewController {
    
    @IBOutlet weak var overlayImageView: UIImageView!
    @IBOutlet weak var messageLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let asset = NSDataAsset(name: "ironman") {
            self.overlayImageView.image = UIImage.gifImageWithData(asset.data)
        }

        // Do any additional setup after loading the view.
    }
    
    /// Dismiss overlay
    func finishOverlay(){
        self.dismiss(animated: true, completion: nil)
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
