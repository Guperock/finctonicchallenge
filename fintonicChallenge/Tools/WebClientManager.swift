//
//  WebClientManager.swift
//  fintonicChallenge
//
//  Created by Jose Eduardo Gutierrez Pastrana on 11/28/19.
//  Copyright © 2019 Jose Eduardo Gutierrez Pastrana. All rights reserved.
//

import UIKit
import Alamofire

///Delegate methods to handler webservices responses
@objc protocol WebClientDelegate {
    @objc optional func finishGetSuperheroData(isSuccess:Bool, superHerosArray:[[String:Any]]?)
    @objc optional func finishDownloadImage(image:UIImage?, imageIdentifierName: String?)
}

/// Class where is all calls to webservices
class WebClientManager: NSObject {
    
    ///Singleton of the class
    public static let client = WebClientManager()
    
    ///Delegate of the class
    var delegate:WebClientDelegate?
        
    private override init() {
    }
    
    ///Consume service that gets superheros json
    func getSuperherosData(){
        let urlService = "https://api.myjson.com/bins/bvyob"
        AF.request(urlService).responseJSON { response in
            switch response.result {
            case .success(let value):
                if let bodyJSON = value as? [String: Any], let superHerosJSONArray = bodyJSON["superheroes"] as? [[String:Any]] {
                    self.delegate?.finishGetSuperheroData?(isSuccess: true, superHerosArray: superHerosJSONArray)
                }
            case .failure(let error):
                print(error)
                self.delegate?.finishGetSuperheroData?(isSuccess: false, superHerosArray: nil)
                // error handling
            }
        }
    }
    
    ///Download image from url and set it to a UIImageView(optional) late hidden placeholderView (optional)
    func downloadImage(imageURL:String, imageIdentifierName:String?, imageViewUpdate: UIImageView?, placeholderView: UIView?){
        AF.download(imageURL).responseData { response in
            var image:UIImage!
            if let data = response.value {
                image = UIImage(data: data)
            }else {
                image = nil
            }
            DispatchQueue.main.async {
                if imageViewUpdate != nil {
                    imageViewUpdate!.image = image
                }
                if placeholderView != nil{
                    placeholderView!.isHidden = true
                }
            }
            
            self.delegate?.finishDownloadImage?(image: image, imageIdentifierName: imageIdentifierName)
        }
    }

}
